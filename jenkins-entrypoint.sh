#!/bin/sh

nohup /usr/local/bin/dockerd-entrypoint.sh &

exec "jenkins-slave" "$@"