FROM jenkins/jnlp-slave:3.36-2-alpine as jenkins

FROM docker:19.03.6-dind

COPY --from=jenkins /usr/share/jenkins/ /usr/share/jenkins/
COPY --from=jenkins /usr/local/bin/jenkins-slave /usr/local/bin/jenkins-slave

COPY jenkins-entrypoint.sh /usr/local/bin/

RUN set -ex; \
    adduser --gecos "" --disabled-password -u "1000" jenkins; \
    mkdir -p ~jenkins/.ssh; \
    echo 'bitbucket.org,104.192.143.1,104.192.143.2,104.192.143.3,104.192.143.65,104.192.143.66,104.192.143.67 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==' >> ~jenkins/.ssh/known_hosts; \
    chmod 700 ~jenkins/.ssh; \
    chmod 644 ~jenkins/.ssh/known_hosts; \
    chown -R jenkins ~jenkins/;

RUN set -ex; \
    apk add --no-cache --update "openjdk8>=8" "git>=2" "openssh-client>=7.9" "nss>=3.41"

USER jenkins

ENTRYPOINT ["jenkins-entrypoint.sh"]

